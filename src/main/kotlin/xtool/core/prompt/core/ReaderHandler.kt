package xtool.core.prompt.core

import java.io.Reader

private val isOldTerminal: Boolean = System.getProperty("os.name").contains("win", ignoreCase = true)

internal interface KInquirerReaderHandler {
    companion object {
        fun getInstance(): KInquirerReaderHandler = when {
            isOldTerminal -> KInquirerReaderHandlerOldTerminal
            else -> KInquirerReaderHandlerNewTerminal
        }
    }

    fun handleInteraction(reader: Reader): KInquirerEvent
}

internal object KInquirerReaderHandlerNewTerminal : KInquirerReaderHandler {

    override fun handleInteraction(reader: Reader): KInquirerEvent {
        return when (val c = reader.read()) {
            127 -> KInquirerEvent.KeyPressBackspace
            13 -> KInquirerEvent.KeyPressEnter
            32 -> KInquirerEvent.KeyPressSpace
            12 -> KInquirerEvent.ClearScreen
            27 -> readEscValues(reader)
            else -> KInquirerEvent.Character(Char(c))
        }
    }

    private fun readEscValues(reader: Reader): KInquirerEvent {
        if (reader.read() == 91) {
            return when (reader.read()) {
                65 -> KInquirerEvent.KeyPressUp    //"↑"
                66 -> KInquirerEvent.KeyPressDown   //"↓"
                67 -> KInquirerEvent.KeyPressRight  //"→"
                68 -> KInquirerEvent.KeyPressLeft   //"←"
                else -> KInquirerEvent.NotSupportedChar
            }
        }
        return KInquirerEvent.NotSupportedChar
    }
}

internal object KInquirerReaderHandlerOldTerminal : KInquirerReaderHandler {
    override fun handleInteraction(reader: Reader): KInquirerEvent {
        return when (val c = reader.read()) {
            8 -> KInquirerEvent.KeyPressBackspace
            13 -> KInquirerEvent.KeyPressEnter
            32 -> KInquirerEvent.KeyPressSpace
            12 -> KInquirerEvent.ClearScreen // (TODO)
            27 -> readEscValues(reader)
            else -> KInquirerEvent.Character(Char(c))
        }
    }

    private fun readEscValues(reader: Reader): KInquirerEvent {
        if (reader.read() == 79) {
            return when (reader.read()) {
                65 -> KInquirerEvent.KeyPressUp     //"↑"
                66 -> KInquirerEvent.KeyPressDown   //"↓"
                67 -> KInquirerEvent.KeyPressRight  //"→"
                68 -> KInquirerEvent.KeyPressLeft   //"←"
                else -> KInquirerEvent.NotSupportedChar
            }
        }
        return KInquirerEvent.NotSupportedChar
    }
}
