package xtool.core.prompt

import xtool.core.prompt.core.AnsiOutput
import xtool.core.prompt.core.Component
import xtool.core.prompt.core.KInquirerReaderHandler
import xtool.core.terminal.TerminalFactory
import java.io.Reader

object Prompt {

    fun <T> prompt(component: Component<T>): T {
        runTerminal { reader ->
            val readerHandler = KInquirerReaderHandler.getInstance()
            AnsiOutput.display(component.render())
            while (component.isInteracting()) {
                val event = readerHandler.handleInteraction(reader)
                component.onEvent(event)
                AnsiOutput.display(component.render())
            }
        }
        return component.value()
    }

    private fun runTerminal(func: (reader: Reader) -> Unit) {
        val terminal = TerminalFactory.terminal
        terminal.enterRawMode()
        val reader = terminal.reader()

        func(reader)

        reader.close()
        terminal.close()
    }
}
