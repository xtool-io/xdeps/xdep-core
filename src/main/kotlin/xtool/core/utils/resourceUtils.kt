package xtool.core.utils

import org.springframework.core.io.Resource
import org.springframework.core.io.support.PathMatchingResourcePatternResolver

/**
 * Retorna uma lista de resources (busca recursiva) do classpath com diretório base definido por source
 * @param source Diretório base para leitura dos resources.
 */
fun readResources(source: String): Array<Resource> = PathMatchingResourcePatternResolver().getResources("classpath:${source}/**/{*.*, .*}")
