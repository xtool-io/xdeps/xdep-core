package xtool.core

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

@Configuration
@ComponentScan
class XdepCore {

    private val log = LoggerFactory.getLogger(XdepCore::class.java)

    @PostConstruct
    fun init() {
        log.debug("xdep-core carregado com sucesso.")
    }
}
