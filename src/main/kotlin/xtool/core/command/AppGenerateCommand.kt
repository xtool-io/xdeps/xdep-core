package xtool.core.command

import org.fusesource.jansi.Ansi
import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.context.WorkspaceContext
import xtool.core.event.AppGenerateEvent
import xtool.core.tasks.FileSystemTask
import java.nio.file.attribute.PosixFilePermission


@Component
@Command(name = "app")
class AppGenerateCommand(
    private val workspaceContext: WorkspaceContext,
    private val fs: FileSystemTask,
    private val appGenerateEvent: AppGenerateEvent,
    private val env: Environment,
) : Runnable {

    private val log = LoggerFactory.getLogger(AppGenerateCommand::class.java)

    override fun run() {
        appGenerateEvent.onBeforeInitialize()
        fs.copyFiles(
            classpathSource = "archetype",
            vars = mapOf(
                "projectName" to workspaceContext.basename
            )
        )
        fs.setPermission(
            workspaceContext.path.resolve("xctl"),
            setOf(
                PosixFilePermission.OWNER_READ,
                PosixFilePermission.OWNER_EXECUTE,
                PosixFilePermission.OWNER_WRITE,
                PosixFilePermission.GROUP_EXECUTE,
                PosixFilePermission.OTHERS_EXECUTE
            )
        )
        appGenerateEvent.onAfterInitialize()
    }
}
