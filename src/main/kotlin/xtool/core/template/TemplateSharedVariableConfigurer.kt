package xtool.core.template

interface TemplateSharedVariableConfigurer {
    fun vars(): Map<String, Any>
}
