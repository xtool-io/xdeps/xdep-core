package xtool.core.config

import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import xtool.core.context.WorkspaceContext
import xtool.core.terminal.InteractiveTerminal
import xtool.core.terminal.NoInteractiveTerminal

@Component
class CommandLineRunnerConfig(
    private val workspaceContext: WorkspaceContext,
    private val noInteractiveTerminal: NoInteractiveTerminal,
    private val interactiveTerminal: InteractiveTerminal
) : CommandLineRunner {
    override fun run(vararg args: String?) {
        when (workspaceContext.isEmpty) {
            true -> noInteractiveTerminal.run(*args)
            false -> interactiveTerminal.run(*args)
        }
    }
}
