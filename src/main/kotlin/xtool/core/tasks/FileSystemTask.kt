package xtool.core.tasks

import org.fusesource.jansi.Ansi
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import xtool.core.context.WorkspaceContext
import xtool.core.utils.readResources
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.attribute.PosixFilePermission

/**
 * Classe com tasks relacionadas a operações com sistema de arquivos.
 */
@Component
class FileSystemTask(private val templateTask: TemplateTask, private val workspaceContext: WorkspaceContext) {

    private val log: Logger = LoggerFactory.getLogger(FileSystemTask::class.java)

    /**
     * Realiza uma cópia com possibilidade de interpolação de arquivos usando o Freemarker.
     * Para habilitar a interpolação é necessário que o arquivo termina com a extensão *.ftl.
     * Os arquivos iniciados com __ não serão copiados para o diretório de destino servido unicamente
     * para inclusão em outros arquivos de template.
     *
     * @param classpathSource String com o diretório do classpath a ser copiado
     * @param destination Path com o caminho onde serão copiados os arquivos.
     * @param vars Mapa com as variáveis que serão interpoladas com os arquivos de template.
     * @param overwrite Se true força a sobrescrita de arquivos caso já existam.
     * @param only Se false a função não será executada.
     */
    fun copyFiles(
        classpathSource: String,
        vars: Map<String, Any>,
        destination: Path = workspaceContext.path,
        overwrite: Boolean = false,
        only: () -> Boolean = { true },
    ) {
        if (!only()) return
        // Carrega todos os arquivos do classpath com diretório base definido por [source]
        // ignorando os arquivos que iniciam com __
        val resources = readResources(classpathSource).filter { !it.filename?.startsWith("__")!! }
        for (resource in resources) {
            if (!resource.isReadable) continue
            // Remove toda a string anterior ao parâmetro source
            val template = resource.url
                .file
                .replace("%7b", "{") // Normaliza o caractere { encoded pela URL
                .replace("%7d", "}") // Normaliza o caractere } encoded pela URL
                .substringAfter(classpathSource)
                .removePrefix("/")
            log.debug("Arquivo de template encontrado: {}", template)
            //
            val finalResourcePath = destination.resolve(
                templateTask.processInline(
                    template.removeSuffix(".ftl"),
                    vars
                )
            )
            if (Files.notExists(finalResourcePath)) {
                // Cria a hierarquia de diretório caso não exista
                if (Files.notExists(finalResourcePath.parent)) Files.createDirectories(finalResourcePath.parent)
                // Caso o arquivo seja um arquivo de template (FreeMarker)
                if (template.endsWith(".ftl")) {
                    val writer = Files.newBufferedWriter(finalResourcePath)
                    templateTask.process(
                        classpathSource,
                        template,
                        writer,
                        vars
                    )
                } else {
                    val os = Files.newOutputStream(finalResourcePath)
                    os.write(resource.inputStream.readBytes())
                    os.flush()
                    os.close()
                }
                log.info("${Ansi.ansi().fgBrightGreen().a("(create)").reset()} ${workspaceContext.path.relativize(Paths.get(finalResourcePath.toString()))}")
                continue
            }
            log.info("${Ansi.ansi().render("@|faint (skip) {} |@").reset()}", workspaceContext.path.relativize(Paths.get(finalResourcePath.toString())))
        }
    }

    fun setPermission(path: Path, permissions: Set<PosixFilePermission>) {
        Files.setPosixFilePermissions(path, permissions)
    }
}
