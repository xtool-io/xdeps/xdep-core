package xtool.core.tasks

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.dircache.DirCache
import org.eclipse.jgit.revwalk.RevCommit
import org.springframework.stereotype.Component
import xtool.core.context.WorkspaceContext
import java.nio.file.Path

/**
 * Classe de service responsável por realizar as operações com o git.
 */
@Component
class GitTask(private val workspaceContext: WorkspaceContext) {

    /**
     * Inicializa o diretório atual com o git.
     */
    fun init(path: Path = workspaceContext.path): Git {
        return Git.init().setDirectory(path.toFile()).call()
    }

    /**
     * Abre o repositório git para operações.
     */
    fun open(path: Path = workspaceContext.path): Git {
        return Git.open(path.toFile())
    }

    /**
     * Realiza a operação add em um repositório git.
     */
    fun add(pattern: String = ".", git: Git = open()): DirCache? {
        val addCommand = git.add()
        addCommand.addFilepattern(pattern)
        return addCommand.call()
    }

    fun commit(message: String, git: Git = open()): RevCommit? {
        val commitCommand = git.commit()
        commitCommand.message = message
        return commitCommand.call()
    }
}
