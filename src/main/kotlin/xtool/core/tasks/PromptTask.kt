package xtool.core.tasks

import org.springframework.stereotype.Component
import xtool.core.prompt.Prompt
import xtool.core.prompt.components.*
import xtool.core.prompt.core.Choice
import xtool.core.prompt.core.ValidationContext
import xtool.core.prompt.factory.preferencesFactory
import java.math.BigDecimal
import java.nio.file.Paths

@Component
class PromptTask {
    fun input(
        message: String,
        default: String = "",
        hint: String = "",
        history: History? = null,
        validation: (s: String) -> ValidationContext = { ValidationContext(true, "") },
        filter: (s: String) -> Boolean = { true },
        transform: (s: String) -> String = { it }
    ): String? {
        var defautValue = default
        history?.let { hist ->
            val prefs = preferencesFactory(Paths.get(".promptrc.xml"))
            prefs?.configuration?.getString(hist.namespace)?.let {
                defautValue = it
            }
        }
        val result = Prompt.promptInput(
            message = message,
            default = defautValue,
            hint = hint,
            validation = validation,
            filter = filter,
            transform = transform
        )
        history?.let { hist ->
            val prefs = preferencesFactory(Paths.get(".promptrc.xml"))
            prefs?.configuration?.setProperty(hist.namespace, result)
            prefs?.save()
        }
        return result
    }

    fun inputPassword(
        message: String,
        default: String = "",
        hint: String = "",
        mask: String = "*",
    ): String {
        return Prompt.promptInputPassword(
            message = message,
            default = default,
            hint = hint,
            mask = mask
        )
    }

    fun promptNumber(
        message: String,
        default: String = "",
        hint: String = "",
        transform: (s: String) -> String = { it },
    ): BigDecimal? {
        return Prompt.promptInputNumber(
            message = message,
            default = default,
            hint = hint,
            transform = transform
        )
    }

    fun promptCheckbox(
        id: String,
        message: String,
        choices: List<String>,
        hint: String = "",
        maxNumOfSelection: Int = Int.MAX_VALUE,
        minNumOfSelection: Int = 0,
        pageSize: Int = 10,
        viewOptions: CheckboxViewOptions = CheckboxViewOptions()
    ): List<String> {
        return Prompt.promptCheckbox(
            message = message,
            choices,
            hint = hint,
            maxNumOfSelection = maxNumOfSelection,
            minNumOfSelection = minNumOfSelection,
            pageSize = pageSize,
            viewOptions = viewOptions
        )
    }

    fun <T> promptCheckboxObject(
        id: String,
        message: String,
        choices: List<Choice<T>>,
        hint: String = "",
        maxNumOfSelection: Int = Int.MAX_VALUE,
        minNumOfSelection: Int = 0,
        pageSize: Int = 10,
        viewOptions: CheckboxViewOptions = CheckboxViewOptions()
    ): List<T> {
        return Prompt.promptCheckboxObject(
            message = message,
            choices = choices,
            hint = hint,
            maxNumOfSelection = maxNumOfSelection,
            minNumOfSelection = minNumOfSelection,
            pageSize = pageSize,
            viewOptions = viewOptions
        )
    }

    fun promptConfirm(
        id: String,
        message: String,
        default: Boolean = false,
    ): Boolean {
        return Prompt.promptConfirm(
            message = message,
            default = default
        )
    }

    fun promptList(
        id: String,
        message: String,
        choices: List<String> = emptyList(),
        hint: String = "",
        pageSize: Int = Int.MAX_VALUE,
        viewOptions: ListViewOptions = ListViewOptions(),
    ): String? {
        return Prompt.promptList(
            message = message,
            choices = choices,
            hint = hint,
            pageSize = pageSize,
            viewOptions = viewOptions
        )
    }

    fun <T> promptListObject(
        id: String,
        message: String,
        choices: List<Choice<T>> = emptyList(),
        hint: String = "",
        pageSize: Int = Int.MAX_VALUE,
        viewOptions: ListViewOptions = ListViewOptions(),
    ): T {
        return Prompt.promptListObject(
            message = message,
            choices = choices,
            hint = hint,
            pageSize = pageSize,
            viewOptions = viewOptions
        )
    }
}
