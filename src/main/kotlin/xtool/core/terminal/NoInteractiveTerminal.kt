package xtool.core.terminal

import org.springframework.stereotype.Component
import picocli.CommandLine
import xtool.core.command.AppGenerateCommand

/**
 * Classe que inicializa o terminal não interativo não lançando o terminal do picocli-jline.
 * Após a execução do comando o prompt de comando do S.O é inicializado novamente.
 *
 */
@Component
class NoInteractiveTerminal(
    private val factory: CommandLine.IFactory,
    private val appGenerateCommand: AppGenerateCommand,
) {

    fun run(vararg args: String?) {
        CommandLine(appGenerateCommand, factory).execute(*args)
    }
}
