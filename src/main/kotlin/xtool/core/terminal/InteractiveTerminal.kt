package xtool.core.terminal

import org.fusesource.jansi.Ansi
import org.fusesource.jansi.AnsiConsole
import org.jline.console.impl.SystemRegistryImpl
import org.jline.reader.EndOfFileException
import org.jline.reader.LineReader
import org.jline.reader.LineReaderBuilder
import org.jline.reader.MaskingCallback
import org.jline.reader.UserInterruptException
import org.jline.reader.impl.DefaultParser
import org.jline.reader.impl.history.DefaultHistory
import org.jline.widget.AutosuggestionWidgets
import org.springframework.stereotype.Component
import picocli.CommandLine
import picocli.CommandLine.IFactory
import picocli.shell.jline3.PicocliCommands
import xtool.core.command.RootCommand
import xtool.core.command.XctlCommand
import xtool.core.context.WorkspaceContext
import java.nio.file.Files
import java.nio.file.Paths
import java.util.function.Supplier

@Component
class InteractiveTerminal(
    private val factory: IFactory,
    private val rootCommand: RootCommand,
    private val workspaceContext: WorkspaceContext,
    private val commands: List<XctlCommand> = listOf()
) {

    fun run(vararg args: String?) {
        AnsiConsole.systemInstall();
        val history = DefaultHistory()
        try {
//            println("""Módulo: ${Ansi.ansi().fgBrightCyan().bold().a(env.getProperty("module.name")).a("@").a(env.getProperty("module.version")).reset()}""")
//            println("")
            val workDir = Paths.get(System.getProperty("user.dir"))
            val pathSupplier = Supplier { workDir }
            // let picocli parse command line args and run the business logic
            val cmd = CommandLine(rootCommand, factory)
            commands.forEach { cmd.addSubcommand(it) }
            val picocliCommands = PicocliCommands(cmd)
            val parser = DefaultParser()
//                val terminal = TerminalBuilder.builder().build()
            val terminal = TerminalFactory.terminal
            val systemRegistry = SystemRegistryImpl(parser, terminal, pathSupplier, null)
            systemRegistry.setCommandRegistries(picocliCommands)
            systemRegistry.register("help", picocliCommands)
            // History
            // Set up history file path
            val historyFilePath = Paths.get(System.getProperty("user.home") + "/.xhistory/${workspaceContext.basename}")
            if (Files.notExists(historyFilePath)) {
                Files.createDirectories(historyFilePath.parent)
                Files.createFile(historyFilePath)
            }
            // Create a history instance

            val reader = LineReaderBuilder.builder()
                .terminal(terminal)
                .history(history)
                .completer(systemRegistry.completer())
                .parser(parser)
                .variable(LineReader.LIST_MAX, 50) // max tab completion candidates
                .variable(LineReader.HISTORY_FILE, historyFilePath)
                .build()
            val widgets = AutosuggestionWidgets(reader)
            widgets.enable()
            val prompt = Ansi.ansi().bold().fgBrightGreen().a("xctl@${workDir.toFile().name}# ").reset().toString()
            val rightPrompt = null
            var line: String?
            while (true) {
                try {
                    systemRegistry.cleanUp()
                    line = reader.readLine(prompt, rightPrompt, null as MaskingCallback?, null)
                    systemRegistry.execute(line)
                } catch (e: UserInterruptException) {
                    // Ignore
                } catch (_: EndOfFileException) {
                    return
                } catch (e: Exception) {
                    systemRegistry.trace(e)
                }
            }
        } catch (t: Throwable) {
            t.printStackTrace();
        } finally {

            AnsiConsole.systemUninstall();
        }
        history.save()
    }
}
