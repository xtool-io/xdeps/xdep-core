package xtool.core.terminal

import org.jline.terminal.Terminal
import org.jline.terminal.TerminalBuilder

object TerminalFactory {
    val terminal: Terminal = TerminalBuilder.builder()
        .jna(true)
        .system(true)
        .build()

}
