package xtool.core.event

interface AppGenerateEvent {

    fun onBeforeInitialize()

    fun onAfterInitialize()
}
