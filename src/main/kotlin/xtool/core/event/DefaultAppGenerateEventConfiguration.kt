package xtool.core.event

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration


@Configuration
class DefaultAppGenerateEventConfiguration : AppGenerateEvent {

    private val log = LoggerFactory.getLogger(DefaultAppGenerateEventConfiguration::class.java)

    override fun onBeforeInitialize() {
        log.debug("onBeforeInitialize()")
    }

    override fun onAfterInitialize() {
        log.debug("onAfterInitialize")
    }
}
