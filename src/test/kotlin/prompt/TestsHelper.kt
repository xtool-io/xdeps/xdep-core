package prompt

import xtool.core.prompt.core.Component
import xtool.core.prompt.core.KInquirerEvent


internal fun <T> Component<T>.onEventSequence(func: MutableList<KInquirerEvent>.() -> Unit) {
    val events = mutableListOf<KInquirerEvent>()
    events.func()
    events.forEach { event -> onEvent(event) }
}
