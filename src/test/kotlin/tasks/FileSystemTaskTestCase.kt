package tasks

import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.info.BuildProperties
import org.springframework.mock.env.MockEnvironment
import xtool.core.context.WorkspaceContext
import xtool.core.tasks.FileSystemTask
import xtool.core.tasks.TemplateTask
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class FileSystemTaskTestCase {

    @BeforeEach
    fun init() {
        FileUtils.deleteDirectory(Paths.get("target/app1").toFile())
        Files.createDirectories(Paths.get("target/app1"))
    }

    @Test
    fun `Teste com cópia de arquivo dummy com interpolação`() {
        val templateTask = TemplateTask(MockEnvironment(), BuildProperties(Properties()), null)
        val workspaceContext = WorkspaceContext(path = Paths.get("target/app1"))
        val fsTask = FileSystemTask(templateTask, workspaceContext)
        fsTask.copyFiles(
            classpathSource = "archetype",
            vars = mapOf(
                "name" to "Fulano"
            )
        )
        Assertions.assertTrue(Files.exists(Paths.get("target/app1/dummy.txt")))
        Assertions.assertEquals("Hello Fulano", String(Files.readAllBytes(Paths.get("target/app1/dummy.txt"))).trim())
    }
}
